import os
from datetime import datetime, timedelta

import telebot
from telebot import types

import nsmart
from model import BusStop, Location, Announcement

BOT_TOKEN = os.getenv('TELEGRAM_BOT_TOKEN')
bot = telebot.TeleBot(BOT_TOKEN)
refresh_cooldown: dict[int, datetime] = {}  # userid: cooldown mapping


@bot.message_handler(commands=['start'])
def send_welcome(message):
    bot.reply_to(message,
                 "Hi! I am bot for finding routes to bus stops. Give me location or "
                 "enter name of bus stop to start search.")


@bot.message_handler(commands=['help'])
def send_help(message):
    help_text = ("How to use bot?\n\n"
                 "Send your location or name of bus stop to bot, select bus stop "
                 "and you will see buses that are coming to that stop.\n"
                 "It might happen that if you send unique name of bus stop, "
                 "then you won't need to select from bus stops, schedule will be shown immediately.")
    bot.reply_to(message, help_text)


@bot.message_handler(commands=['share_my_location'])
def request_location(message):
    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    location_button = types.KeyboardButton(text="Send Location", request_location=True)
    markup.add(location_button)
    bot.reply_to(message, "Please share your location", reply_markup=markup)


def send_location(chat_id: int, latitude: float, longitude: float, stop: BusStop | None = None):
    if stop is None:
        bot.send_location(chat_id, latitude, longitude)
        return

    markup = types.InlineKeyboardMarkup()
    button = types.InlineKeyboardButton(stop.name, callback_data=f"id_{stop.uuid}")
    markup.add(button)
    bot.send_location(chat_id, latitude, longitude, reply_markup=markup)


@bot.message_handler(content_types=['location'])
def handle_location(message):
    user_location = Location(message.location.latitude, message.location.longitude)
    nearest_stops = nsmart.find_nearest_stops(user_location, 5)
    if not nearest_stops:
        bot.reply_to(message, "No nearby bus stops found.")
        return

    bot.send_message(message.chat.id, "Select bus stop:")
    for stop_distance in nearest_stops:
        stop = stop_distance.stop
        bot.send_message(message.chat.id, f"{stop.name} - {stop_distance.distance:.2f} km away")
        send_location(message.chat.id, stop.location.latitude, stop.location.longitude, stop)


@bot.message_handler(func=lambda message: len(message.text) >= 5)
def find_stop(message):
    result = nsmart.find_bus_stops_by_name(message.text)

    if len(result) == 0:
        bot.reply_to(message, "No bus stops found with this name, or no stop with this name has valid GPS coordinates")
        return

    if len(result) == 1:
        bot.reply_to(message, parse_announcements(nsmart.get_announcement_data(result[0].uuid)))
        return

    bot.send_message(message.chat.id, "Select bus stop:")
    for stop_info in result:
        send_location(message.chat.id, stop_info.get_latitude(), stop_info.get_longitude(), stop_info)


@bot.message_handler(func=lambda message: len(message.text) < 5)
def short_message_warning(message):
    bot.reply_to(message, "Please enter word with at least 5 characters.")


@bot.callback_query_handler(func=lambda call: call.data.startswith("id_"))
def handle_callback_query(call):
    stop_id = call.data.split("_")[1]
    try:
        announcement_data = parse_announcements(nsmart.get_announcement_data(stop_id))

        keyboard = types.InlineKeyboardMarkup()
        refresh_button = types.InlineKeyboardButton("Refresh", callback_data=f"refresh_{stop_id}")
        keyboard.add(refresh_button)

        bot.send_message(call.message.chat.id, announcement_data, reply_markup=keyboard)

    except Exception as e:
        error = "Unknown Error: " + str(e)
        bot.send_message(call.message.chat.id, error)
    finally:
        bot.answer_callback_query(call.id)


@bot.callback_query_handler(func=lambda call: call.data.startswith("refresh_"))
def handle_refresh_query(call):
    user_id = call.from_user.id
    stop_id = call.data.split("_")[1]

    if user_id in refresh_cooldown:
        last_refresh_time = refresh_cooldown[user_id]
        if (datetime.now() - last_refresh_time) < timedelta(seconds=3):
            return

    try:
        announcement_data = parse_announcements(nsmart.get_announcement_data(stop_id))

        keyboard = types.InlineKeyboardMarkup()
        refresh_button = types.InlineKeyboardButton("Refresh", callback_data=f"refresh_{stop_id}")
        keyboard.add(refresh_button)

        current_time = datetime.now().strftime("%H:%M:%S %d.%m.%Y")
        updated_message_text = f"{announcement_data}\n\nLast refreshed: {current_time}"

        bot.edit_message_text(
            chat_id=call.message.chat.id,
            message_id=call.message.message_id,
            text=updated_message_text,
            reply_markup=keyboard
        )

        refresh_cooldown[user_id] = datetime.now()

    except Exception as e:
        error = "Unknown Error: " + str(e)
        bot.send_message(call.message.chat.id, error)


def parse_announcements(announcements: list[Announcement]) -> str:

    if len(announcements) == 0:
        return "No complete bus route information available for this stop."

    announcement_format = """
    ========= route information =========
    Bus: %s number
    Route: %s
    %s
    
    """

    result = []
    for a in announcements:
        if a.minutes_left() == 0:
            eta_line = f"Bus will be here in: {a.minutes_left} minutes"
        else:
            eta_line = "Bus should already be at stop"
        result.append(announcement_format % (a.line_number, a.main_line_title, eta_line))

    return "\n".join(result)


bot.polling()
