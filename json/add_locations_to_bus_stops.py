import json

import requests

headers = {'X-Api-Authentication': '4670f468049bbee2260'}

with open('updated_novisad_bus_stops.json', 'r') as file:
    bus_stops = json.load(file)

for stop in bus_stops:
    if stop['stations_gpsx'] is None and stop['stations_gpsy'] is None:
        try:
            response = requests.get(
                f"https://online.nsmart.rs/publicapi/v1/announcement/announcement.php?station_uid={stop['id']}",
                headers=headers
            )
            if response.status_code == 200:
                data = response.json()
                if 'stations_gpsx' in data[0] and 'stations_gpsy' in data[0]:
                    stop['stations_gpsx'] = data[0]['stations_gpsx']
                    stop['stations_gpsy'] = data[0]['stations_gpsy']
            else:
                print(f"Error with station ID {stop['id']}: {response.status_code}")
        except json.JSONDecodeError:
            print(f"Invalid JSON response for station ID {stop['id']}")
        except requests.RequestException as e:
            print(f"Request error for station ID {stop['id']}: {e}")

with open('updated_novisad_bus_stops.json', 'w', encoding='utf-8') as file:
    json.dump(bus_stops, file, indent=4, ensure_ascii=False)
