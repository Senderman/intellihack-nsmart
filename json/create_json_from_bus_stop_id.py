import json

import requests

station_ids = [
    1111, 2222
]

json_data = [
    {
        "id": station_id,
        "bus_stop_name": None,
        "unique_bus_stop_name": None,
        "stations_gpsx": None,
        "stations_gpsy": None
    } for station_id in station_ids
]

url_base = "https://online.nsmart.rs/publicapi/v1/announcement/announcement.php?station_uid="
headers = {'X-Api-Authentication': '4670f468049bbee2260'}

for station in json_data:
    station_id = station["id"]
    response = requests.get(url_base + str(station_id), headers=headers)
    if response.status_code == 200:
        data = response.json()
        if data and data[0].get("station_name"):
            station["bus_stop_name"] = data[0]["station_name"]

json_data = [station for station in json_data if station["bus_stop_name"] is not None]

bus_stop_names = [station["bus_stop_name"] for station in json_data]
for station in json_data:
    station["unique_bus_stop_name"] = 1 if bus_stop_names.count(station["bus_stop_name"]) == 1 else 0

file_path = 'new_stations_id.json'

with open(file_path, 'w', encoding='utf-8') as file:
    json.dump(json_data, file, indent=4, ensure_ascii=False)
